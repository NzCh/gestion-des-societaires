<?php
use App\Controllers;
use Core\Http\Request;
//use Core\Http\Response;
use Phalcon\Http\Response;
use App\Models;


class ClientPhysiqueController extends PrivateController {
       
/**********************************************************************************/
//                           Ajouter client physique   

// POST:http://localhost:8000/rsu/ClientPhysique/addClientPhysique
/**********************************************************************************/
    public function addClientPhysiqueAction() {
       
        $request = new Request();
        $errors=[];
        $params=[];
       
        $params['id_qualite'] = $this->request->getPost('id_qualite');
     
        $params['nom_client'] = $this->request->getPost('nom_client');

        if ((empty($params['nom_client'])) || (!preg_match('/^[A-Z ]{3,16}$/', $params['nom_client']))) {
                $errors['nom_client'] = 'le Nom du sociétaire accepte seulement les lettres de A à Z
                ne peut pas être vide';
        }        
 
        $params['prenom_client'] = $this->request->getPost('prenom_client');
        if ((empty($params['prenom_client'])) || (!preg_match('/^[A-Z ]{3,16}$/', $params['prenom_client']))){
            $errors['prenom_client'] ='le Prenom du sociétaire accepte seulement les lettres de A à Z
            ne peut pas être vide';
        }

        $params['id_sexe_client'] = $this->request->getPost('id_sexe_client');
        // if ((empty($params['sexe_client'])) || (!preg_match('/^[F-H]{1}$/', $params['sexe_client']))){
        //     $errors['sexe_client'] ='le sexe est invalide saisir H pour homme ou F pour femme';
        // }
        
        $params['date_naissance'] = $this->request->getPost('date_naissance');

        $params['id_profession'] = $this->request->getPost('id_profession');

        $params['telephone_mobile'] = $this->request->getPost('telephone_mobile');
        if ((empty($params['telephone_mobile'])) || (!preg_match('/^[0-9 ]{1,10}$/', $params['telephone_mobile']))) {
            $errors['telephone_mobile'] = 'le telephone mobile ne peut pas être vide
            Entrez un numero de téléphone valide (10 chiffres)';
        }

        if (!empty($params['telephone_fix'] = $this->request->getPost('telephone_fix'))){
            if (!preg_match('/^[0-9]{1,14}$/', $params['telephone_fix'])){
                    $errors['telephone_fix'] = 'le Telephone Fix doit consiste seulement des nombres';
            }
        }
        else {
            $params['telephone_fix'] = $this->request->getPost('telephone_fix' ,null,' ');
        }

        $params['email'] = $this->request->getPost('email');
        if ((empty($params['email']))) {
            $errors['email'] = 'Email est obligatoire saisir une forme valid Ex:exemple@gmail.com';
        }

        $params['id_organisme_affiliation'] = $this->request->getPost('id_organisme_affiliation'); 

        $params['reference_appartenance'] = $this->request->getPost('reference_appartenance');
         if ((empty($params['reference_appartenance'])) || (!preg_match('/^[A-Z0-9 ]{2,15}$/', $params['reference_appartenance']))) {
            $errors['reference_appartenance']  = 'la reference d\'appartenance du sociétaire est obligatoire saisir des alphabets en majuscule';
        }
        
        // $params['is_vip'] = $this->request->getPost('is_vip'); 
        // if(!preg_match('/^[0-1]{1}$/', $params['is_vip'] )){
        //     $errors['is_vip'] = 'Choisir 1 pour Sociétaire Vip sinon 0';
        // }

        $params['id_vip'] = $this->request->getPost('id_vip');

        $params['id_categorie_vip'] = $this->request->getPost('id_categorie_vip');

        $params['reference_document'] = $this->request->getPost('reference_document');
        if ((empty($params['reference_document'])) || (!preg_match('/^[A-Z0-9]{2,30}$/', $params['reference_document'])) ) {
            $errors['reference_document'] = 'Entrez un numero de la carte nationale valide sans espaces
            ne peut pas être vide';
        }
   
        $params['date_delivrance'] = $this->request->getPost('date_delivrance',null,' ');

        $params['lieu_delivrance'] = $this->request->getPost('lieu_delivrance');
        if ((empty($params['lieu_delivrance'])) || (!preg_match('/^[A-Z ]{2,30}$/', $params['lieu_delivrance'])) ) {
            $errors['lieu_delivrance'] = 'le lieu de delivrance accepte seulement les lettres de A à Z
            ne peut pas être vide';
        }

        $params['date_expiration'] = $this->request->getPost('date_expiration',null,' ');


        if (!empty($params['numero_rc'] = $this->request->getPost('numero_rc'))){
            if (!preg_match('/^[A-Z0-9 ]{1,15}$/', $params['numero_rc'])){
                    $errors['numero_rc'] = 'le Numéro de registre du commerce doit contient des lettres en majuscule ou des nombres';
                }
        }
        else {
            $params['numero_rc'] = $this->request->getPost('numero_rc',null,' ');
        }

        if (!empty($params['tribunale_immatriculation'] = $this->request->getPost('tribunale_immatriculation'))){
            if (!preg_match('/^[A-Z0-9 ]{2,50}$/', $params['tribunale_immatriculation'])){
                    $errors['tribunale_immatriculation'] = 'le Tribunal d\'immatriculation doit contient des lettres en majuscule ou des nombres';
                }
        }
        else {
            $params['tribunale_immatriculation'] = $this->request->getPost('tribunale_immatriculation',null,' ');
        }
           
        $params['type_adresse'] = $this->request->getPost('type_adresse');
        if ((empty($params['type_adresse'])) || (!preg_match('/^[R-U]{1}$/', $params['type_adresse']))){
            $errors['type_adresse'] ='taper R pour Rural ou U pour Urbaine';
        }

        if (!empty($params['numero'] = $this->request->getPost('numero'))){
            if (!preg_match('/^[0-9]{2,100}$/', $params['numero'])){
                    $errors['numero'] = 'le numero de votre adresse  doit consiste seulement des nombres';
                }
        }
        else {
            $params['numero'] = $this->request->getPost('numero',null,' ');
        }


        $params['id_voie'] = $this->request->getPost('id_voie');
        // if ((!isset($data['id_voie'])) && (empty($data['id_voie']))) {
        //     $params['id_voie'] = null;
        // }
        // else {
        //     $params['id_voie'] = $this->request->getPost('id_voie'); 
        // }

        $params['complement_adresse'] = $this->request->getPost('complement_adresse');
        if ((empty($params['complement_adresse'])) || (!preg_match('/^[A-Z0-9 ]{2,15}$/', $params['complement_adresse']))) {
            $errors['complement_adresse']  = 'Erreur le complement d\'adresse doit consiste  des lettres en majuscule';
        }

        if (!empty($params['code_postal'] = $this->request->getPost('code_postal'))){
            if (!preg_match('/^[0-9]{2,100}$/', $params['code_postal'])){
                    $errors['code_postal'] = 'Votre Code Postal  doit consiste seulement des nombres';
            }
        }
        else {
            $params['code_postal'] = $this->request->getPost('code_postal',null,' ');
        }
        

        $params['id_ville'] = $this->request->getPost('id_ville');

        $params['id_pays_naissance'] = $this->request->getPost('id_pays_naissance');
      
        if (!empty($params['kiyada'] = $this->request->getPost('kiyada'))){
            if (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['kiyada'])){
                    $errors['kiyada'] = 'kiyada doit consiste des lettres en majuscule';
            }
        }
        else {
            $params['kiyada'] = $this->request->getPost('kiyada',null,' ');
        }

        if (!empty($params['douar'] = $this->request->getPost('douar'))){
            if (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['douar'])){
                    $errors['douar'] = 'Douar doit consiste des lettres en majuscule';
            }
        }
        else {
            $params['douar'] = $this->request->getPost('douar',null,' ');
        }

        // $params['id_province'] = $this->request->getPost('id_province');
        
        // $params['id_commune'] = $this->request->getPost('id_commune');
        
        $params['id_activite'] = $this->request->getPost('id_activite');
       
        if ($errors) {
            $response = new Response();
            $response->setContent(json_encode($errors));
        }

        else {
        $response_data = ClientPhysiqueModel::addClientPhysique($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        }
        return $response;
    
    }


/**********************************************************************************/
//                          Modifier client physique  
//  PUT:http://localhost:8000/rsu/ClientPhysique/updateClientPhysique/id_client_physique/5
/**********************************************************************************/    


public function updateClientPhysiqueAction() {

    $request = new Request();
    $errors=[];
    $params=[];
    
    $params['id_qualite'] = $this->request->getPut('id_qualite');

    $params['nom_client'] = $this->request->getPut('nom_client');
    if ((empty($params['nom_client'])) || (!preg_match('/^[A-Z ]{3,16}$/', $params['nom_client']))) {
        $errors['nom_client'] = 'le Nom du sociétaire est obligatoire saisir des alphabets en majuscule';
    }

    $params['prenom_client'] = $this->request->getPut('prenom_client');
    if ((empty($params['prenom_client'])) || (!preg_match('/^[A-Z ]{3,16}$/', $params['prenom_client']))){
        $errors['prenom_client'] ='le Prenom du sociétaire du sociétaire est obligatoire saisir des alphabets en majuscule';
    }

    $params['id_sexe_client'] = $this->request->getPut('id_sexe_client');
    // if ((empty($params['sexe_client'])) || (!preg_match('/^[F-H]{1}$/', $params['sexe_client']))){
    //     $errors['sexe_client'] ='le sexe est invalide saisir H pour homme ou F pour femme';
    // }
    
    $params['date_naissance'] = $this->request->getPut('date_naissance');

    $params['id_profession'] = $this->request->getPut('id_profession');

    $params['telephone_mobile'] = $this->request->getPut('telephone_mobile');
    if ((empty($params['telephone_mobile'])) || (!preg_match('/^[0-9 ]{1,12}$/', $params['telephone_mobile']))) {
        $errors['telephone_mobile'] = 'le telephone mobile est obligatoir saisir des nombres';
    }

    if (!empty($params['telephone_fix'] = $this->request->getPut('telephone_fix'))){
        if (!preg_match('/^[0-9]{1,14}$/', $params['telephone_fix'])){
                $errors['telephone_fix'] = 'le Telephone Fix doit consiste seulement des nombres';
        }
    }
    else {
        $params['telephone_fix'] = $this->request->getPut('telephone_fix' ,null,' ');
    }

    $params['email'] = $this->request->getPut('email');
    if ((empty($params['email']))) {
        $errors['email'] = 'Email est obligatoire saisir une forme valid Ex:exemple@gmail.com';
    }

    $params['id_organisme_affiliation'] = $this->request->getPut('id_organisme_affiliation'); 

    $params['reference_appartenance'] = $this->request->getPut('reference_appartenance');
     if ((empty($params['reference_appartenance'])) || (!preg_match('/^[A-Z0-9 ]{2,15}$/', $params['reference_appartenance']))) {
        $errors['reference_appartenance']  = 'la reference d\'appartenance du sociétaire est obligatoire saisir des alphabets en majuscule';
    }
    
    // $params['is_vip'] = $this->request->getPut('is_vip'); 
    // if(!preg_match('/^[0-1]{1}$/', $params['is_vip'] )){
    //     $errors['is_vip'] = 'Choisir 1 pour Sociétaire Vip sinon 0';
    // }

    $params['id_vip'] = $this->request->getPut('id_vip');

    $params['id_categorie_vip'] = $this->request->getPut('id_categorie_vip');

    $params['reference_document'] = $this->request->getPut('reference_document');
    if ((empty($params['reference_document'])) || (!preg_match('/^[A-Z0-9]{2,30}$/', $params['reference_document'])) ) {
        $errors['reference_document'] = 'le CIN est obligatoire saisir des lettres en majuscule et des nombres sans espace';
    }

    $params['date_delivrance'] = $this->request->getPut('date_delivrance',null,' ');

    $params['lieu_delivrance'] = $this->request->getPut('lieu_delivrance');
        if ((empty($params['lieu_delivrance'])) || (!preg_match('/^[A-Z ]{2,30}$/', $params['lieu_delivrance'])) ) {
            $errors['lieu_delivrance'] = 'le lieu de delivrance est obligatoire saisir des lettres en majuscule';
        }

    $params['date_expiration'] = $this->request->getPut('date_expiration',null,' ');

    if (!empty($params['numero_rc'] = $this->request->getPut('numero_rc'))){
        if (!preg_match('/^[A-Z0-9 ]{1,15}$/', $params['numero_rc'])){
                $errors['numero_rc'] = 'le Numéro de registre du commerce doit contient des lettres en majuscule ou des nombres';
            }
    }
    else {
        $params['numero_rc'] = $this->request->getPut('numero_rc',null,' ');
    }

    if (!empty($params['tribunale_immatriculation'] = $this->request->getPut('tribunale_immatriculation'))){
        if (!preg_match('/^[A-Z0-9 ]{2,50}$/', $params['tribunale_immatriculation'])){
                $errors['tribunale_immatriculation'] = 'le Tribunal d\'immatriculation doit contient des lettres en majuscule ou des nombres';
            }
    }
    else {
        $params['tribunale_immatriculation'] = $this->request->getPut('tribunale_immatriculation',null,' ');
    }
       
    $params['type_adresse'] = $this->request->getPut('type_adresse');
    if ((empty($params['type_adresse'])) || (!preg_match('/^[R-U]{1}$/', $params['type_adresse']))){
        $errors['type_adresse'] ='taper R pour Rural ou U pour Urbaine';
    }

    if (!empty($params['numero'] = $this->request->getPut('numero'))){
        if (!preg_match('/^[0-9]{2,100}$/', $params['numero'])){
                $errors['numero'] = 'le numero de votre adresse  doit consiste seulement des nombres';
            }
    }
    else {
        $params['numero'] = $this->request->getPut('numero',null,' ');
    }

    $params['id_voie'] = $this->request->getPut('id_voie');
    // if ((!isset($data['id_voie'])) || (empty($params['id_voie']))) {
    //    // $params['id_voie'] = $this->request->getPost('id_voie',null,null);
    //    $params['id_voie'] = null;
    // }

    $params['complement_adresse'] = $this->request->getPut('complement_adresse');
    if ((empty($params['complement_adresse'])) || (!preg_match('/^[A-Z0-9 ]{2,15}$/', $params['complement_adresse']))) {
        $errors['complement_adresse']  = 'Erreur le complement d\'adresse doit consiste  des lettres en majuscule';
    }

    if (!empty($params['code_postal'] = $this->request->getPut('code_postal'))){
        if (!preg_match('/^[0-9]{2,100}$/', $params['code_postal'])){
                $errors['code_postal'] = 'Votre Code Postal  doit consiste seulement des nombres';
        }
    }
    else {
        $params['code_postal'] = $this->request->getPut('code_postal',null,' ');
    }
    
    $params['id_ville'] = $this->request->getPut('id_ville');

    $params['id_pays_naissance'] = $this->request->getPut('id_pays_naissance');
  
    if (!empty($params['kiyada'] = $this->request->getPut('kiyada'))){
        if (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['kiyada'])){
                $errors['kiyada'] = 'kiyada doit consiste des lettres en majuscule';
        }
    }
    else {
        $params['kiyada'] = $this->request->getPut('kiyada',null,' ');
    }

    if (!empty($params['douar'] = $this->request->getPut('douar'))){
        if (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['douar'])){
                $errors['douar'] = 'Douar doit consiste des lettres en majuscule';
        }
    }
    else {
        $params['douar'] = $this->request->getPut('douar',null,' ');
    }

    // $params['id_province'] = $this->request->getPost('id_province');
    
    // $params['id_commune'] = $this->request->getPost('id_commune');
    
    $params['id_activite'] = $this->request->getPut('id_activite');
   
    //$params['id_client_physique'] = $this->request->getPut('id_client_physique');
     $params['id_client_physique'] = $this->dispatcher->getParam('id_client_physique');
    //$params['id_client_physique'] = $id_client_physique;
   
    if ($errors) {
        $response = new Response();
        $response->setContent(json_encode($errors));
    }

    else {
        $response_data = ClientPhysiqueModel::updateClientPhysique($params);      
        $response = new Response();
        $response->setContent(json_encode($response_data));
    }
    return $response;
    }


/**********************************************************************************/
//                          Récuperer tous les  clients physique  

//    GET:http://localhost:8000/rsu/ClientPhysique/getlisteClientsPhysique                            |
/**********************************************************************************/  

    public function getlisteClientsPhysiqueAction() {
 
        $response_data = ClientPhysiqueModel::selectAllClientsPhysique();
        $result['ClientsPhysique'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;

    }

/********************************************************************************************************************/
//       Afficher la fiche sociétaire d'un client Physique On utilisons Id_client_physique
//GET : http://localhost:8000/rsu/ClientPhysique/getClientPhysiqueByIdClientPhysique/id_client_physique/3 (give id_client_physique to get Client)
/********************************************************************************************************************/    

    public function getClientPhysiqueByIdClientPhysiqueAction() {

        //$request = new Request();  
        $id_client_physique = $this->dispatcher->getParam('id_client_physique');
        $response_data = ClientPhysiqueModel::selectClientPhysique($id_client_physique);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;

    } 
   
/********************************************************************************************************************/
//      Supprimer un client Physique On utilisons Id_client_physique
//DELETE : http://localhost:8000/rsu/ClientPhysique/deleteClientPhysiqueByIdClientPhysique/id_client_physique/5
/********************************************************************************************************************/   

    public function deleteClientPhysiqueByIdClientPhysiqueAction() {

       // $request = new Request();  
        //$params['id_client_physique'] = $this->request->getPost('id_client_physique');
        $params['id_client_physique'] = $this->dispatcher->getParam('id_client_physique');
        $response_data = ClientPhysiqueModel::deleteClientPhysique($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;

    } 

/********************************************************************************************************************/
//       Chercher un client physique par le nom ou bien la reference d'appartenance ou bien télephone
//Post : http://localhost:8000/rsu/SearchClientPhysique/searchClientPhysiqueByNamaReferanceTelephone
/********************************************************************************************************************/    

public function searchClientPhysiqueByNamaReferanceTelephoneAction() {
   
    $request = new Request(); 
    $reference_appartenance = $this->request->getPost('reference_appartenance',null,' ');
    $nom_client = $this->request->getPost('nom_client',null,' ');
    $telephone_mobile = $this->request->getPost('telephone_mobile',null,' ');
    $reference_document = $this->request->getPost('reference_document',null,' ');
    $response_data = ClientPhysiqueModel::searchClientPhysique("'".$reference_appartenance."'","'".$nom_client."'","'".$telephone_mobile."'","'".$reference_document."'");
    $response = new Response();
    $response->setContent(json_encode($response_data));
    return $response;

}


}