<?php

use Phalcon\Mvc\Model;
use Core\Db\Database;
use App\Controllers;

class ClientPhysiqueModel extends Model {
    
    public static function addClientPhysique($params){
        $db = new Database();
        $sp = '[dbo].[ps_creer_client_physique]';
        $result = $db->execSP($sp, $params ); 
        return $result;

    }

    public static function updateClientPhysique($params){
        $db = new Database();
        $sp = '[dbo].[ps_modifier_client_physique]';
        $result=  $db->execSP($sp,$params);
        return $result;
    }


    public static function selectAllClientsPhysique(){
        
        $db = new Database();
        $sql = 'SELECT id_client_physique, p.nom_client,prenom_client,x.id_sexe_client,sexe_client,date_naissance,telephone_mobile,telephone_fix,email,reference_appartenance,p.id_qualite,qualite,p.id_profession,profession,p.id_organisme_affiliation,nom_organisme_affiliation,c.id_client,e.id_vip,is_vip,c.id_categorie_vip,categorie_vip, uuid_client, date_creation,type_adresse,numero,complement_adresse,code_postal,a.id_ville,nom_ville,a.id_voie,libelle_voie,a.id_pays_naissance,libelle_pays,id_client_identifiant,tribunale_immatriculation,numero_rc, date_creation,reference_document,t.id_type_document,t.type_document,lieu_delivrance,date_expiration,date_delivrance from [dbo].[cli_client_physique]  p
        join [dbo].[cli_sexe_client] x on x.id_sexe_client = p.id_sexe_client
        join [dbo].[cli_clients]  c on p.id_client = c.id_client
        join std_qualites  q on p.id_qualite = q.id_qualite
        join cli_professions  f on f.id_profession = p.id_profession
        join cli_vip e on e.id_vip = c.id_vip
        join cli_organisme_affiliation  o on p.id_organisme_affiliation = o.id_organisme_affiliation
        join cli_categorie_vip  v on c.id_categorie_vip = v .id_categorie_vip
        join cli_adresses  a on a.id_client = c.id_client
        join std_villes  l on l.id_ville = a.id_ville
        join std_voies  s on s.id_voie = a.id_voie
        join std_pays  y on y.id_pays_naissance = a.id_pays_naissance
        join cli_client_identifiants  n on n.id_client = p.id_client
        JOIN cli_piece_identite w  ON  c.id_client = w.id_client
        JOIN [dbo].[cli_type_document] t ON t.id_type_document = w.id_type_document  AND t.id_type_document = 1';
        $result = $db->selectAll($sql);
        return $result;

    }

    public static function selectClientPhysique($id_client_physique){
        
        $db = new Database();
        $sql = 'SELECT id_client_physique, p.nom_client,prenom_client,x.id_sexe_client,sexe_client,date_naissance,telephone_mobile,telephone_fix,email,reference_appartenance,p.id_qualite,qualite,p.id_profession,profession,p.id_organisme_affiliation,nom_organisme_affiliation,c.id_client,e.id_vip,is_vip,c.id_categorie_vip,categorie_vip, date_creation,type_adresse,numero,complement_adresse,code_postal,a.id_ville,nom_ville,a.id_voie,libelle_voie,a.id_pays_naissance,libelle_pays,id_client_identifiant,tribunale_immatriculation,numero_rc, date_creation,reference_document,t.id_type_document,t.type_document,lieu_delivrance,date_expiration,date_delivrance,uuid_client from [dbo].[cli_client_physique]  p
        join [dbo].[cli_sexe_client] x on x.id_sexe_client = p.id_sexe_client   
        join [dbo].[cli_clients] c on p.id_client = c.id_client
        join std_qualites q on p.id_qualite = q.id_qualite
        join cli_professions f on f.id_profession = p.id_profession
        join cli_organisme_affiliation o on p.id_organisme_affiliation = o.id_organisme_affiliation
        join cli_vip e on e.id_vip = c.id_vip
        join cli_categorie_vip v on c.id_categorie_vip = v .id_categorie_vip
        join cli_adresses a on a.id_client = c.id_client
        join std_villes l on l.id_ville = a.id_ville
        join std_voies s on s.id_voie = a.id_voie
        join std_pays y on y.id_pays_naissance = a.id_pays_naissance
        join cli_client_identifiants n on n.id_client = p.id_client
        join cli_piece_identite w  ON  c.id_client = w.id_client
        join [dbo].[cli_type_document] t ON t.id_type_document = w.id_type_document  AND t.id_type_document = 1
        where p.id_client_physique = ('.$id_client_physique.')';
        $result = $db->selectAll($sql);
        return $result;

    }

 
    public static function deleteClientPhysique($params) {
        
        $db = new Database();
        $sp = '[dbo].[ps_delete_client_physique]';
        $result=  $db->execSP($sp,$params);
        return $result;

    }
    
    public static function searchClientPhysique($reference_appartenance,$nom_client,$telephone_mobile,$reference_document) {
   
        $db = new Database();
        $sql = 'SELECT id_client_physique, p.nom_client,prenom_client,x.id_sexe_client,sexe_client,date_naissance,telephone_mobile,telephone_fix,email,reference_appartenance,p.id_qualite,qualite,p.id_profession,profession,p.id_organisme_affiliation,nom_organisme_affiliation,c.id_client,e.id_vip,is_vip,c.id_categorie_vip,categorie_vip, date_creation,type_adresse,numero,complement_adresse,code_postal,a.id_ville,nom_ville,a.id_voie,libelle_voie,a.id_pays_naissance,libelle_pays,id_client_identifiant,tribunale_immatriculation,numero_rc, date_creation,reference_document,t.id_type_document,t.type_document,lieu_delivrance,date_expiration,date_delivrance from [dbo].[cli_client_physique]  p
        join [dbo].[cli_sexe_client] x on x.id_sexe_client = p.id_sexe_client 
        join [dbo].[cli_clients] c on p.id_client = c.id_client
        join std_qualites q on p.id_qualite = q.id_qualite
        join cli_professions f on f.id_profession = p.id_profession
        join cli_organisme_affiliation o on p.id_organisme_affiliation = o.id_organisme_affiliation
        join cli_vip e on e.id_vip = c.id_vip
        join cli_categorie_vip v on c.id_categorie_vip = v .id_categorie_vip
        join cli_adresses a on a.id_client = c.id_client
        join std_villes l on l.id_ville = a.id_ville
        join std_voies s on s.id_voie = a.id_voie
        join std_pays y on y.id_pays_naissance = a.id_pays_naissance
        join cli_client_identifiants n on n.id_client = p.id_client 
        join cli_piece_identite w  ON  c.id_client = w.id_client
        join [dbo].[cli_type_document] t ON t.id_type_document = w.id_type_document  AND t.id_type_document = 1
        where reference_appartenance = '.$reference_appartenance.'  OR  p.nom_client = '.$nom_client.' OR p.telephone_mobile = ('.$telephone_mobile.') OR w.reference_document = ('.$reference_document.')';
        $result = $db->selectAll($sql);
        return $result;
    
    }


     

}