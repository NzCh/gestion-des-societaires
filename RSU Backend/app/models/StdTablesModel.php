<?php

use Phalcon\Mvc\Model;
use Core\Db\Database;
use App\Controllers;

class StdTablesModel extends Model {
    
//------------- Récupérer tous les éléments d'une table de nomenclature -------------//    
    
    //Villes
    public static function getListeVilles(){
        
        $db = new Database();
        $sql = 'select id_ville, nom_ville from [dbo].[std_villes]';
        $result = $db->selectAll($sql); 
        return $result;

    }

    public static function addVille($params) {
        
        $db = new Database();
        $sp = '[dbo].[ps_ajouter_ville]';
        $result=  $db->execSP($sp,$params);
        return $result;
    }

    //Secteurs
    public static function getListeSecteurs(){
       
        $db = new Database();
        $sql = 'select id_secteur, secteur  from [dbo].[std_secteur]';
        $result = $db->selectAll($sql); 
        return $result;
    
    }
    
    public static function addSecteur($params) {
            
        $db = new Database();
        $sp = '[dbo].[ps_ajouter_secteur]';
        $result=  $db->execSP($sp,$params);
        return $result;

    }

    //Filieres
    public static function getListeFilieres(){
       
        $db = new Database();
        $sql = 'select id_filiere, filiere, id_secteur  from [dbo].[std_filiere]';
        $result = $db->selectAll($sql); 
        return $result;
    
    }

    public static function addFiliere($params) {
            
        $db = new Database();
        $sp = '[dbo].[ps_ajouter_filiere]';
        $result=  $db->execSP($sp,$params);
        return $result;

    }    
        
    //Activites
    public static function getListeActivites(){
        
        $db = new Database();
        $sql = 'select id_activite, activite, id_filiere from [dbo].[std_activites]';
        $result = $db->selectAll($sql); 
        return $result;

    }

    public static function addActivite($params) {
        
        $db = new Database();
        $sp = '[dbo].[ps_ajouter_activite]';
        $result=  $db->execSP($sp,$params);
        return $result;
    }

    //Communes
    public static function getListeCommunes(){
       
        $db = new Database();
        $sql = 'select id_commune, commune from [dbo].[std_communes]';
        $result = $db->selectAll($sql); 
        return $result;

    }

    public static function addCommune($params) {
        
        $db = new Database();
        $sp = '[dbo].[ps_ajouter_commune]';
        $result=  $db->execSP($sp,$params);
        return $result;
    }

    //Pays
    public static function getListePays(){
       
        $db = new Database();
        $sql = 'select id_pays_naissance, libelle_pays from [dbo].[std_pays]';
        $result = $db->selectAll($sql); 
        return $result;

    }

    public static function addPays($params) {
        
        $db = new Database();
        $sp = '[dbo].[ps_ajouter_pays]';
        $result=  $db->execSP($sp,$params);
        return $result;
    }

    //Qualites
    public static function getListeQualites(){
       
        $db = new Database();
        $sql = 'select id_qualite, qualite from [dbo].[std_qualites]';
        $result = $db->selectAll($sql); 
        return $result;

    }

    public static function addQualite($params) {
        
        $db = new Database();
        $sp = '[dbo].[ps_ajouter_qualite]';
        $result=  $db->execSP($sp,$params);
        return $result;
    }

     //Voies
     public static function getListeVoies(){
       
        $db = new Database();
        $sql = 'select id_voie, libelle_voie from [dbo].[std_voies]';
        $result = $db->selectAll($sql); 
        return $result;

    }

    public static function addVoie($params) {
        
        $db = new Database();
        $sp = '[dbo].[ps_ajouter_voie]';
        $result=  $db->execSP($sp,$params);
        return $result;
    }

    //Organismes d'Affiliation
    public static function getListeOrganismesAffiliation(){
       
        $db = new Database();
        $sql = 'select id_organisme_affiliation, nom_organisme_affiliation from [dbo].[cli_organisme_affiliation]';
        $result = $db->selectAll($sql); 
        return $result;

    }

    public static function addOrganismeAffiliation($params) {
        
        $db = new Database();
        $sp = '[dbo].[ps_ajouter_organisme]';
        $result=  $db->execSP($sp,$params);
        return $result;
    }

     //Categories VIP
     public static function getListeCategorieVip(){
       
        $db = new Database();
        $sql = 'select id_categorie_vip, categorie_vip  from [dbo].[cli_categorie_vip]';
        $result = $db->selectAll($sql); 
        return $result;

    }

    public static function addCategorieVip($params) {
        
        $db = new Database();
        $sp = '[dbo].[ps_ajouter_ctgvip]';
        $result=  $db->execSP($sp,$params);
        return $result;
    }    

    
     //Professions
     public static function getListeProfessions(){
       
        $db = new Database();
        $sql = 'select id_profession, profession  from [dbo].[cli_professions]';
        $result = $db->selectAll($sql); 
        return $result;

    }

    public static function addProfession($params) {
        
        $db = new Database();
        $sp = '[dbo].[ps_ajouter_profession]';
        $result=  $db->execSP($sp,$params);
        return $result;
    }


    //Formes Juridiques
    public static function getListeFormesJuridiques(){
       
        $db = new Database();
        $sql = 'select id_forme_juridique, forme_juridique  from [dbo].[std_forme_juridique]';
        $result = $db->selectAll($sql); 
        return $result;

    }

    public static function addFormeJuridique($params) {
        
        $db = new Database();
        $sp = '[dbo].[ps_ajouter_formejuridique]';
        $result=  $db->execSP($sp,$params);
        return $result;
    }

    //VIP
    public static function getListeVip(){
       
            $db = new Database();
            $sql = 'select id_vip, is_vip from [dbo].[cli_vip]';
            $result = $db->selectAll($sql); 
            return $result;
    
    }

    //Type document
    public static function getListeTypeDocument(){
       
            $db = new Database();
            $sql = 'select id_type_document, type_document from [dbo].[cli_type_document]';
            $result = $db->selectAll($sql); 
            return $result;
    
    }

    //Province
    public static function getListeProvinces(){
       
        $db = new Database();
        $sql = 'select id_province, province from [dbo].[std_provinces]';
        $result = $db->selectAll($sql); 
        return $result;
     }

     //Activité secteur filiere
     public static function getListeAFS(){
      
        $db = new Database();
        $sql = 'SELECT  f.id_filiere, filiere, s.id_secteur,secteur,id_activite, activite  from [dbo].[std_activites] a 
        join [dbo].[std_filiere] f on f.id_filiere = a.id_filiere 
        join [dbo].[std_secteur] s on s.id_secteur = f.id_filiere';
        $result = $db->selectAll($sql); 
        return $result;
        
     }

    //Sexe client
    public static function getListeSexeClient() {
      
        $db = new Database();
        $sql = 'SELECT  id_sexe_client, sexe_client  from [dbo].[cli_sexe_client] ';
        $result = $db->selectAll($sql); 
        return $result ;
        
    }

    //ajouter Activite filiere secteur
    public static function addActiviteSecteurFiliere($params) {
        
        $db = new Database();
        $sp = '[dbo].[ps_creer_asf]';
        $result=  $db->execSP($sp,$params);
        return $result ;

    }


    




}