<?php

use Phalcon\Mvc\Model;
use Core\Db\Database;
use App\Controllers;

class ClientMoraleModel extends Model {
    
    public static function addClientMorale($params){
        $db = new Database();
        $sp = '[dbo].[ps_creer_client_morale]';
        $result = $db->execSP($sp, $params ); 
        return $result;

    }

    public static function updateClientMorale($params){
        $db = new Database();
        $sp = '[dbo].[ps_modifier_client_morale]';
        $result = $db->execSP($sp, $params ); 
        return $result;

    }

    public static function selectAllClient() {
        
        $db = new Database();
        $sql = 'SELECT id_client_morale,c.id_client,raison_sociale,nom_abreg,f.id_forme_juridique,forme_juridique,tel_fixe,fax,siteweb,nom_holding,e.id_vip,is_vip,uuid_client,c.id_categorie_vip,categorie_vip,id_client_identifiant,numero_rc,tribunale_immatriculation,n.identifiant_fiscale,numero_patente,ice,type_adresse,numero,s.id_voie,libelle_voie,complement_adresse,code_postal,l.id_ville,nom_ville,y.id_pays_naissance,libelle_pays from [dbo].[cli_client_morale] m
        join [dbo].[cli_clients] c on c.id_client = m.id_client
        join [dbo].[std_forme_juridique] f on f.id_forme_juridique = m.id_forme_juridique
        join cli_vip e on e.id_vip = c.id_vip
        join [dbo].[cli_categorie_vip] v on c.id_categorie_vip = v .id_categorie_vip
        join [dbo].[cli_client_identifiants] n on n.id_client = c.id_client 
        join [dbo].[cli_adresses] a on a.id_client = c.id_client
        join [dbo].[std_villes] l on l.id_ville = a.id_ville
        join [dbo].[std_voies] s on s.id_voie = a.id_voie
        join [dbo].[std_pays] y on y.id_pays_naissance = a.id_pays_naissance';
        $result = $db->selectAll($sql);
        return $result;

    }

    public static function searchClientMoraleByIce($ice) {
   
        $db = new Database();
        $sql = 'SELECT id_client_morale,c.id_client,raison_sociale,nom_abreg,f.id_forme_juridique,forme_juridique,tel_fixe,fax,siteweb,nom_holding,e.id_vip,is_vip,c.id_categorie_vip,categorie_vip,id_client_identifiant,numero_rc,tribunale_immatriculation,n.identifiant_fiscale,numero_patente,ice,type_adresse,numero,s.id_voie,libelle_voie,complement_adresse,code_postal,l.id_ville,nom_ville,y.id_pays_naissance,libelle_pays from [dbo].[cli_client_morale] m
        join [dbo].[cli_clients] c on c.id_client = m.id_client
        join [dbo].[std_forme_juridique] f on f.id_forme_juridique = m.id_forme_juridique
        join cli_vip e on e.id_vip = c.id_vip
        join [dbo].[cli_categorie_vip] v on c.id_categorie_vip = v .id_categorie_vip
        join [dbo].[cli_client_identifiants] n on n.id_client = c.id_client 
        join [dbo].[cli_adresses] a on a.id_client = c.id_client
        join [dbo].[std_villes] l on l.id_ville = a.id_ville
        join [dbo].[std_voies] s on s.id_voie = a.id_voie
        join [dbo].[std_pays] y on y.id_pays_naissance = a.id_pays_naissance
        where ice = ('.$ice.')';
        $result = $db->selectAll($sql);
        return $result;
    
       }

}